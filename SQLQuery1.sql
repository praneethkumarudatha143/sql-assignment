---ASSIGNMENT_4
---Task1 Database Design

--creating user table

create table user_table(UserID INT PRIMARY KEY, Name VARCHAR(255), Email VARCHAR(255) UNIQUE, Password VARCHAR(255), ContactNumber VARCHAR(20), Address TEXT)

--Inserting data into user_table

INSERT INTO user_table VALUES
(1, 'Ram', 'Ram@gmail.com', 'password123', '9100367892', 'Whitefields,Bangalore'),
(2, 'Ravi', 'Ravi@gmail.com', 'pass456', '9876543210', '5th street,xyz colony,tirupathi'),
(3, 'Kiran', 'Kiran@gmail.com', 'abc123', '8349939393', '3RD strt,gandhi nagar,guntur'),
(4, 'vinay', 'vinay@gmail.com', 'securepass', '8456790237', '4th street,thyagaraj nagar,nellore'),
(5, 'praveen', 'praveen@gmail.com', 'evapass', '8967888999', '6th street,ramnagar,rajahmundry'),
(6, 'vinod', 'vinod@gmail.com', 'password123', '8120367892', '6th street,abc colony,vijaywada'),
(7, 'vikram', 'vikram@gmail.com', 'pass456', '9126543210', '4th rd,perumal rd,kerala'),
(8, 'Kavitha', 'Kavitha@gmail.com', 'abc123', '9049939393', 'near maalakshmi temple,kovur'),
(9, 'vamsi', 'vamsi@gmail.com', 'securepass', '6356790237', 'keerthi avenue,ongole'),
(10, 'punith', 'punith@gmail.com', 'evapass', '9067888999', '2nd street,tnagar,chennai')

--creating courier table

create table Courier(CourierID INT PRIMARY KEY, SenderName VARCHAR(255), SenderAddress TEXT, ReceiverName VARCHAR(255), ReceiverAddress TEXT, Weight DECIMAL(5, 2), Status VARCHAR(50), TrackingNumber VARCHAR(20) UNIQUE, DeliveryDate DATE)

--Inserting data into Courier Table

INSERT INTO Courier(CourierID, SenderName, SenderAddress, ReceiverName, ReceiverAddress, Weight, Status, TrackingNumber, DeliveryDate) VALUES
(101, 'Praneeth', '123 St,ram nagar', 'Ravi', '5th street,xyz colony,tirupathi', 3.5, 'Delivered', 'TN123456', '2024-01-20'),
(102, 'Mouniker', '789 Rd,gandhi nagar', 'Ram', 'Whitefields,Bangalore', 2.2, 'Delivered', 'TN789012', '2024-01-22'),
(103, 'keerthi', '456 Rd,sita nagar', 'Rahul', '654  St,raghav colony', 5.0, 'Pending', 'TN345678', '2024-01-25'),
(104, 'shanmuk', '111 Rd,smr colony', 'Rehman', '222  Rd,jyothi colony', 1.8, 'Delivered', 'TN901234', '2024-01-18'),
(105, 'Ram', 'Whitefields,Bangalore', 'kiran', '555 st,avmnagar', 4.7, 'Pending', 'TN567890', '2024-01-30'),
(106, 'Prajwesh', '123 St,ramaya nagar,guntur', 'Ravi', '5th street,xyz colony,tirupathi', 3.5, 'Delivered', 'TN123457', '2024-01-20'),
(107, 'Mounika', '782 Rd,gandhi nagar,guntur', 'Ram', 'Whitefields,Bangalore', 2.2, 'Delivered', 'TN789067', '2024-01-22'),
(108, 'vikram', '4th rd,perumal rd,keralar', 'Rahul', '654  St,raghav colony', 5.0, 'Pending', 'TN345677', '2024-01-25'),
(109, 'vamsi', 'keerthi avenue,ongole', 'Kavitha', 'near maalakshmi temple,kovur', 1.8, 'Delivered', 'TN901236', '2024-01-18'),
(110, 'Ram', 'Whitefields,Bangalore', 'kiran', '555 st,avmnagar', 4.7, 'Pending', 'TN567823', '2024-01-30')

--Since some relationships are misiing we are adding ServiceID,EmployeeID,USerID as foreign keys


alter table Courier add  ServiceID INT,EmployeeID INT,UserID INT FOREIGN KEY (ServiceID) REFERENCES  CourierServices(ServiceID),FOREIGN KEY (EmployeeID) REFERENCES  Employee(EmployeeID),UserID INT FOREIGN KEY (UserID) REFERENCES  User_table(UserID)

--updating those newly added columns with values

Update Courier set ServiceID=201,EmployeeID=302,UserID=3 where CourierID=101
Update Courier set ServiceID=202,EmployeeID=302,UserID=3 where CourierID=102
Update Courier set ServiceID=201,EmployeeID=303,UserID=6 where CourierID=103
Update Courier set ServiceID=203,EmployeeID=305,UserID=4 where CourierID=104
Update Courier set ServiceID=203,EmployeeID=307,UserID=1 where CourierID=105
Update Courier set ServiceID=208,EmployeeID=308,UserID=5 where CourierID=106
Update Courier set ServiceID=209,EmployeeID=310,UserID=6 where CourierID=107
Update Courier set ServiceID=201,EmployeeID=302,UserID=7 where CourierID=108
Update Courier set ServiceID=205,EmployeeID=305,UserID=9 where CourierID=109
Update Courier set ServiceID=210,EmployeeID=305,UserID=1 where CourierID=110


--creating courierServices table

create table CourierServices(ServiceID INT PRIMARY KEY, ServiceName VARCHAR(100), Cost DECIMAL(8, 2))

--inserting values into CourierServices table

INSERT INTO CourierServices (ServiceID, ServiceName, Cost) VALUES
(201, 'Standard Delivery', 15.99),
(202, 'Express Delivery', 29.99),
(203, 'Same Day Delivery', 79.99),
(204, 'International Delivery', 39.99),
(205, 'Priority Delivery', 49.99),
(206, 'International Priority', 59.99),
(207, 'Two-Day Shipping', 24.99),
(208, 'Local Pickup', 0.00),
(209, 'Two-Hour Delivery', 49.99),
(210, 'Weekend Express',34.99 )

--create a Employee Table

create table Employee(EmployeeID INT PRIMARY KEY, Name VARCHAR(255), Email VARCHAR(255) UNIQUE, ContactNumber VARCHAR(20), Role VARCHAR(50), Salary DECIMAL(10, 2))

--Inserting values into Employee table

INSERT INTO Employee(EmployeeID, Name, Email, ContactNumber, Role, Salary) VALUES
(301, 'johnparker', 'johnparke@gmail.com', '9456789023', 'Manager', 75000.00),
(302, 'Kishore', 'Kishore@gmail.com', '7345091234', 'DeliveyBoy', 15000.00),
(303, 'Murthy', 'Murthy@gmail.com', '8345012345', 'DeliveyBoy', 15000.00),
(304, 'Arjun', 'Arjun@gmail.com', '7834560123', 'Clerk', 8000.00),
(305, 'Devi', 'Devi@gmail.com', '9876340123', 'DeliveyBoy', 18000.00),
(306, 'Vidya', 'Vidya@gmail.com', '9456067023', 'Clerk', 8000.00),
(307, 'Nani', 'Nani@gmail.com', '7345033234', 'DeliveyBoy', 15000.00),
(308, 'Suresh', 'Suresh@gmail.com', '8330512345', 'DeliveyBoy', 15000.00),
(309, 'johncarl', 'johncarl@gmail.com', '7094560123', 'DeliveyBoy', 18000.00),
(310, 'Ramya','Ramya@gmail.com', '9876945123', 'DeliveyBoy', 18000.00)

--creating Location Table

create table Location(LocationID INT PRIMARY KEY, LocationName VARCHAR(100), Address TEXT)

--Inserting values in location table

INSERT INTO Location (LocationID, LocationName, Address) VALUES
(401, 'vijaywada', '6th street,abc colony'),
(402, 'tirupathi', '5th street,xyz colony'),
(403, 'nellore', '4th street,thyagaraj nagar'),
(404, 'guntur', '3rd steet,gandhi nagar'),
(405, 'rajahmundry', '6th street,ramnagar'),
(406, 'kovur', 'near maalakshmi temple'),
(407, 'ongole', 'keerthi avenue'),
(408, 'kerala', '4th rd,perumal rd'),
(409, 'chennai', '2nd street,t nagar'),
(410, 'bangalore', 'Whitefields')

--creating payment table
create table Payment(PaymentID INT PRIMARY KEY, CourierID INT, LocationId INT, Amount DECIMAL(10, 2), PaymentDate DATE, FOREIGN KEY (CourierID) REFERENCES Courier(CourierID), FOREIGN KEY (LocationID) REFERENCES Location(LocationID))

--Inserting values in Payment

INSERT INTO Payment (PaymentID, CourierID, LocationID, Amount, PaymentDate) VALUES
(501, 101, 401, 500, '2024-01-21'),
(502, 102, 402, 2000, '2024-01-23'),
(503, 103, 407, 3000, '2024-01-26'),
(504, 104, 401, 1000, '2024-01-19'),
(505, 101, 405, 4000, '2024-01-31'),
(506, 108, 403, 500, '2024-01-21'),
(507, 102, 408, 2000, '2024-01-23'),
(508, 104, 403, 3000, '2024-01-26'),
(509, 105, 404, 1000, '2024-01-19'),
(510, 109, 408, 4000, '2024-01-31')


--Task 2: Select,Where

--1. List all customers:
select *from user_table

--2.List all orders for a specific customer:
select *from courier where SenderName='Ram'

--3. List all couriers:
select *from Courier

--4.. List all packages for a specific order:(qs mistake)


--5. List all deliveries for a specific courier:
SELECT cs.ServiceID, cs.ServiceName, c.CourierID FROM CourierServices cs JOIN Courier c on cs.ServiceID = c.ServiceId
--6. List all undelivered packages: 
select *from Courier where Status='Pending'

--7. List all packages that are scheduled for delivery today: 
SELECT *FROM Courier WHERE CONVERT(DATE, Courier.DeliveryDate) = CONVERT(DATE, GETDATE());

--8. List all packages with a specific status
select *from Courier where Status='Delivered'
--9. Calculate the total number of packages for each courier. 
SELECT cs.ServiceID,cs.ServiceName,COUNT(c.CourierID) AS TotalCouriers FROM CourierServices cs
LEFT JOIN Courier c ON cs.ServiceID = c.ServiceID
GROUP BY cs.ServiceID, cs.ServiceName

--10. Find the average delivery time for each courier 
SELECT  AVG(DAY(Delivery_Date)) as Average_delivery_time FROM Courier

--11. List all packages with a specific weight range:
select *from Courier where Weight between 3.00 and 5.00

--12. Retrieve employees whose names contain 'John'
select *from Employee where Name Like '%john%'

--13. Retrieve all courier records with payments greater than $50.
select *from courier where CourierID IN (select distinct CourierID from Payment where Amount>50)

--Task 3: GroupBy, Aggregate Functions, Having, Order By, where 

--14. Find the total number of couriers handled by each employee.
SELECT Employee.EmployeeID, Employee.Name AS EmployeeName, COUNT(Courier.CourierID) AS TotalCouriersHandled FROM Employee
LEFT JOIN Courier ON Employee.EmployeeID = Courier.EmployeeID
GROUP BY Employee.EmployeeID, Employee.Name

--15. Calculate the total revenue generated by each location
SELECT Location.LocationID, Location.LocationName, SUM(Payment.Amount) AS TotalRevenue FROM Location
LEFT JOIN Payment on Location.LocationID = Payment.LocationID
GROUP BY Location.LocationID, Location.LocationName

--16. Find the total number of couriers delivered to each location. 
SELECT LocationID,COUNT(*) AS TotalCouriersDelivered FROM Courier
WHERE Status = 'Delivered' GROUP BY LocationID;

--17. Find the courier with the highest average delivery time: 
SELECT TOP 1 CourierID, AVG(DATEDIFF(DAY, DeliveryDate, GETDATE())) AS AverageDeliveryTime FROM Courier
WHERE Status = 'Delivered' GROUP BY CourierID
ORDER BY AverageDeliveryTime DESC;

--18. Find Locations with Total Payments Less Than a Certain Amount 
SELECT LocationID,SUM(Amount)
FROM Payment
GROUP BY LocationID
HAVING SUM(Amount)<1500

--19. Calculate Total Payments per Location
SELECT LocationID,SUM(Amount) AS TotalPayments
FROM Payment
GROUP BY LocationID;


--20. Retrieve couriers who have received payments totaling more than $1000 in a specific location 
(LocationID = X): 
SELECT Courier.CourierID,Courier.SenderName,Courier.ReceiverName,Courier.Status,Payment.Amount
FROM Courier JOIN Payment ON Courier.CourierID = Payment.CourierID
WHERE Payment.LocationID =408 AND Payment.Amount > 1000;



--21. Retrieve couriers who have received payments totaling more than $1000 after a certain date (PaymentDate > 'YYYY-MM-DD'): 
SELECT Courier.CourierID
FROM Courier
JOIN Payment ON Courier.CourierID = Payment.CourierID
WHERE Payment.PaymentDate > '2024-01-21'
GROUP BY Courier.CourierID
HAVING SUM(Payment.Amount) > 1000

--22. Retrieve locations where the total amount received is more than $5000 before a certain date (PaymentDate > 'YYYY-MM-DD') 
SELECT Payment.LocationID
FROM Location
JOIN Payment ON Location.LocationID = Payment.LocationID
WHERE Payment.PaymentDate < '2024-01-31'
GROUP BY Payment.LocationID
HAVING SUM(Payment.Amount) > 5000

--Task 4: Inner Join,Full Outer Join, Cross Join, Left Outer Join,Right Outer Join 

--23. Retrieve Payments with Courier Information 
SELECT Payment.*, Courier.* FROM Payment JOIN Courier ON Payment.CourierID = Courier.CourierID

--24. Retrieve Payments with Location Information 
SELECT Payment.*, Location.LocationName,Location.Address FROM Payment JOIN Location ON Payment.LocationID = Location.LocationID

--25. Retrieve Payments with Courier and Location Information 
SELECT Payment.*, Courier.*,location.* FROM Payment JOIN Courier ON Payment.CourierID = Courier.CourierID JOIN Location ON Payment.LocationID = Location.LocationID

--26. List all payments with courier details 
SELECT Payment.*, Courier.*
FROM Payment
JOIN Courier ON Payment.CourierID = Courier.CourierID

--27. Total payments received for each courier 
SELECT Courier.CourierID, SUM(Payment.Amount) AS TotalPayments
FROM Courier
LEFT JOIN Payment ON Courier.CourierID = Payment.CourierID
GROUP BY Courier.CourierID;


--28. List payments made on a specific date
SELECT *
FROM Payment
WHERE PaymentDate = '2024-01-21'

--29. Get Courier Information for Each Payment 
SELECT Payment.*, Courier.*
FROM Payment
JOIN Courier ON Payment.CourierID = Courier.CourierID;


--30. Get Payment Details with Location 
SELECT Payment.*, Location.LocationName
FROM Payment
JOIN Location ON Payment.LocationID = Location.LocationID


--31. Calculating Total Payments for Each Courier
SELECT Courier.CourierID, SUM(Payment.Amount) AS TotalPayments
FROM Courier
LEFT JOIN Payment ON Courier.CourierID = Payment.CourierID
GROUP BY Courier.CourierID

--32. List Payments Within a Date Range
SELECT * FROM Payment WHERE PaymentDate BETWEEN '2024-01-01' AND '2024-01-23'

--33. Retrieve a list of all users and their corresponding courier records, including cases where there are no matches on either side 
select user.*,courier.* from user_table FULL JOIN courier ON user_table.ID = courier.CourierID

--34. Retrieve a list of all couriers and their corresponding services, including cases where there are no matches on either side 
SELECT Courier.*,CourierServices.* FROM Courier
FULL JOIN CourierServices ON Courier.ServiceID = CourierServices.ServiceID;

--35. Retrieve a list of all employees and their corresponding payments, including cases where there are  no matches on either side (#qs not clear)


--36. List all users and all courier services, showing all possible combinations. 
SELECT user_table.UserID, User_table.Name AS UserName, CourierServices.ServiceID, CourierServices.ServiceName, CourierServices.Cost
FROM User_table
CROSS JOIN CourierServices

--37. List all employees and all locations, showing all possible combinations: 
SELECT Employee.EmployeeID, Employee.Name AS EmployeeName, Location.LocationID, Location.LocationName
FROM Employee
CROSS JOIN Location;

--38. Retrieve a list of couriers and their corresponding sender information (if available) 
select courier.*,user_table.* from Courier JOIN user_table ON Courier.userID=user_table.UserID

--39. Retrieve a list of couriers and their corresponding receiver information (if available): 
select  courierID,ReceiverName,ReceiverAddress from courier

--40. Retrieve a list of couriers along with the courier service details (if available): 
SELECT Courier.*, ServiceName,Cost FROM Courier JOIN CourierServices ON Courier.ServiceID = CourierServices.ServiceID

--41. Retrieve a list of employees and the number of couriers assigned to each employee: 
SELECT Employee.EmployeeID, Employee.Name, COUNT(Courier.CourierID) AS NumberOfCouriers FROM Employee LEFT JOIN Courier ON Employee.EmployeeID = Courier.EmployeeID
GROUP BY Employee.EmployeeID, Employee.Name

--42. Retrieve a list of locations and the total payment amount received at each location: 
SELECT Location.LocationID, Location.LocationName, SUM(Payment.Amount) AS TotalPaymentAmount
FROM Location
LEFT JOIN Payment ON Location.LocationID = Payment.LocationID
GROUP BY Location.LocationID, Location.LocationName

--43. Retrieve all couriers sent by the same sender (based on SenderName).
SELECT *
FROM Courier
WHERE SenderName = 'Ram'


--44. List all employees who share the same role.
select *from Employee where Role='clerk'

--45. Retrieve all payments made for couriers sent from the same location.
SELECT p1.* FROM Payment P1 JOIN Payment p2 ON p1.PaymentID = p2.PaymentID AND p2.LocationId = 408

--46. Retrieve all couriers sent from the same location (based on SenderAddress).
SELECT a.CourierID, a.SenderName, a.SenderAddress, a.ReceiverName, a.ReceiverAddress, a.Weight, a.Status, a.TrackingNumber, a.ServiceId 
FROM Courier a JOIN Courier b ON a.CourierID = b.CourierID AND CONVERT(VARCHAR, b.SenderAddress) = 'Whitefields,Bangalore'

--47. List employees and the number of couriers they have delivered: 
SELECT Employee.EmployeeID, Employee.Name AS EmployeeName, COUNT(Courier.CourierID) AS NumberOfCouriersDelivered FROM Employee
LEFT JOIN Courier ON Employee.EmployeeID = Courier.EmployeeID
GROUP BY Employee.EmployeeID, Employee.Name


--48. Find couriers that were paid an amount greater than the cost of their respective courier services
SELECT Courier.*, Payment.Amount, CourierServices.Cost FROM Courier
JOIN CourierServices ON Courier.ServiceID = CourierServices.ServiceID
JOIN Payment ON Courier.CourierID = Payment.CourierID
WHERE Payment.Amount > CourierServices.Cost;

--Scope: Inner Queries, Non Equi Joins, Equi joins,Exist,Any,All 

--49. Find couriers that have a weight greater than the average weight of all couriers 
SELECT *FROM Courier WHERE Weight > ALL(SELECT AVG(Weight) FROM Courier)

--50. Find the names of all employees who have a salary greater than the average salary: 
SELECT Name FROM Employee WHERE Salary > (SELECT AVG(Salary) FROM Employee)

--51. Find the total cost of all courier services where the cost is less than the maximum cost 
SELECT SUM(Cost) AS TotalCost FROM CourierServices WHERE Cost < (SELECT MAX(Cost) FROM CourierServices)

--52. Find all couriers that have been paid for 
select *from  Courier where CourierID IN(Select CourierID from payment)

--53. Find the locations where the maximum payment amount was made
select locationID,sum(Amount) AS Maximum_Payment FROM Payment GROUP BY LocationId ORDER BY SUM(AMOUNT) DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY


--54. Find all couriers whose weight is greater than the weight of all couriers sent by a specific sender (e.g., 'SenderName'): 
SELECT *FROM Courier c1 WHERE Weight > ALL (SELECT Weight FROM Courier c2 WHERE c2.SenderName = 'Ram')



