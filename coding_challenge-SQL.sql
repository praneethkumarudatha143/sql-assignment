--Coding Challenge SQL
--Crime Management Shema DDL and DML

CREATE TABLE Crime (CrimeID INT PRIMARY KEY,IncidentType VARCHAR(255),IncidentDate DATE,Location VARCHAR(255),
Description TEXT,Status VARCHAR(20));CREATE TABLE Victim (VictimID INT PRIMARY KEY,CrimeID INT,Name VARCHAR(255),
ContactInfo VARCHAR(255),Injuries VARCHAR(255),FOREIGN KEY (CrimeID) REFERENCES Crime(CrimeID));

CREATE TABLE Suspect (SuspectID INT PRIMARY KEY,CrimeID INT,Name VARCHAR(255),Description TEXT,
CriminalHistory TEXT,FOREIGN KEY (CrimeID) REFERENCES Crime(CrimeID));-- Insert sample data

INSERT INTO Crime (CrimeID, IncidentType, IncidentDate, Location, Description, Status)
VALUES
 (1, 'Robbery', '2023-09-15', '123 Main St, Cityville', 'Armed robbery at a convenience store', 'Open'),
 (2, 'Homicide', '2023-09-20', '456 Elm St, Townsville', 'Investigation into a murder case', 'Under
Investigation'),
 (3, 'Theft', '2023-09-10', '789 Oak St, Villagetown', 'Shoplifting incident at a mall', 'Closed'); INSERT INTO Victim (VictimID, CrimeID, Name, ContactInfo, Injuries)
VALUES
 (1, 1, 'John Doe', 'johndoe@example.com', 'Minor injuries'),
 (2, 2, 'Jane Smith', 'janesmith@example.com', 'Deceased'), (3, 3, 'Alice Johnson', 'alicejohnson@example.com', 'None'); --we are adding age column to Victim Table alter table Victim ADD Age INTupdate Victim SET Age=35 where VictimID=1update Victim SET Age=28 where VictimID=2update Victim SET Age=49 where VictimID=3 INSERT INTO Suspect (SuspectID, CrimeID, Name, Description, CriminalHistory)
VALUES
 (1, 1, 'Robber 1', 'Armed and masked robber', 'Previous robbery convictions'),
 (2, 2, 'Unknown', 'Investigation ongoing', NULL),
 (3, 3, 'Suspect 1', 'Shoplifting suspect', 'Prior shoplifting arrests')alter table Suspect ADD Age INTupdate Suspect SET Age=36 where SuspectID=1update Suspect SET Age=34 where SuspectID=2update Suspect SET Age=49 where SuspectID=3
--Solve the below queries:

--1. Select all open incidents.
select *from Crime where Status='Open'

--2. Find the total number of incidents.
Select COUNT(*) AS total_incidents FROM Crime

--3. List all unique incident types.
Select DISTINCT IncidentType FROM Crime

--4. Retrieve incidents that occurred between '2023-09-01' and '2023-09-10'.
Select * FROM Crime WHERE IncidentDate BETWEEN '2023-09-01' AND '2023-09-10'

--5. List persons involved in incidents in descending order of age.
Select Name, Age FROM Victim ORDER BY Age DESC

--6. Find the average age of persons involved in incidents.
Select AVG(Age) AS average_age FROM Victim WHERE Age IS NOT NULL

--7. List incident types and their counts, only for open cases.
Select IncidentType, COUNT(*) AS count FROM Crime WHERE Status = 'Open' GROUP BY IncidentType

--8. Find persons with names containing 'Doe'.
Select Name FROM Victim WHERE Name LIKE '%Doe%'

--9. Retrieve the names of persons involved in open cases and closed cases.
Select V.Name AS PersonName,C.Status AS CaseStatus FROM Victim V
JOIN Crime C ON V.CrimeID = C.CrimeID
WHERE C.Status IN ('Open','Closed')

--10. List incident types where there are persons aged 30 or 35 involved.
Select DISTINCT C.IncidentType FROM Crime C JOIN Victim V ON C.CrimeID = V.CrimeID WHERE V.Age IN (30, 35)


--11. Find persons involved in incidents of the same type as 'Robbery'.
Select V.* FROM Victim V JOIN Crime C ON V.CrimeID = C.CrimeID WHERE C.IncidentType = 'Robbery'

--12. List incident types with more than one open case.
SELECT IncidentType, COUNT(*) AS OpenCases FROM Crime WHERE Status = 'Open' GROUP BY IncidentType HAVING COUNT(*) > 1

--13. List all incidents with suspects whose names also appear as victims in other incidents.
Select *FROM Crime AS C JOIN Victim AS V ON C.CrimeID = V.CrimeID
JOIN Suspect AS S ON C.CrimeID = S.CrimeID AND V.Name = S.Name

--14. Retrieve all incidents along with victim and suspect details.
Select C.*, V.Name AS VictimName, V.ContactInfo, V.Injuries, S.Name AS SuspectName, S.Description AS SuspectDescription, S.CriminalHistory
FROM Crime AS C LEFT JOIN Victim AS V ON C.CrimeID = V.CrimeID LEFT JOIN Suspect AS S ON C.CrimeID = S.CrimeID

--15. Find incidents where the suspect is older than any victim.
Select C.*FROM Crime C JOIN Victim V ON C.CrimeID = V.CrimeID JOIN Suspect S ON C.CrimeID = S.CrimeID
WHERE S.Age > ALL (SELECT Age FROM Victim WHERE CrimeID = C.CrimeID)


--16. Find suspects involved in multiple incidents:
Select Name,COUNT(*) AS IncidentCount From Suspect GROUP BY Name HAVING COUNT(*) > 1

--17. List incidents with no suspects involved.
Select *from Crime WHERE CrimeID NOT IN (SELECT CrimeID FROM Suspect)

--18. List all cases where at least one incident is of type 'Homicide' and all other incidents are of type
'Robbery'.
Select *FROM Crime WHERE IncidentType = 'Homicide' OR (IncidentType = 'Robbery' AND CrimeID NOT IN (Select CrimeID FROM Crime WHERE IncidentType = 'Homicide'))

--19. Retrieve a list of all incidents and the associated suspects, showing suspects for each incident, or
'No Suspect' if there are none.

SELECT c.*,COALESCE(s.SuspectID, 0) AS SuspectID,COALESCE(s.Name, 'No Suspect') AS SuspectName,COALESCE(s.Description, 'No Suspect') AS SuspectDescription,
COALESCE(s.CriminalHistory, 'No Suspect') AS CriminalHistory,COALESCE(s.Age, 0) AS SuspectAge
FROM Crime c LEFT JOIN Suspect s ON c.CrimeID = s.CrimeID

--20. List all suspects who have been involved in incidents with incident types 'Robbery' or 'Assault'select S.* from Suspect S JOIN Crime C ON S.CrimeID = C.CrimeID WHERE C.IncidentType IN ('Robbery','Assault')

